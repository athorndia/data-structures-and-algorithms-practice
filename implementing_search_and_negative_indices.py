'''
The code below contains the singly linked list implemented so far.
Take a look through the code.

Feel free to create an instance of the MySLL object and use print statements
to explore what happens when different arguments are passed.
Pay special attention to what happens when negative indices are passed
as arguments. It's probably not the behavior you were expecting, and
certainly not the intended behavior.

Implement a 'search' method and modify the code so that negative arguments
passed to insert, traverse, and remove methods behave as they would in a
list or array.
'''


class MyNode:

    def __init__(self, value=None, link=None):
        self.value = value
        self.link = link

    def __str__(self):
        return f"Node with value:{self.value}"


class MySLL:

    def __init__(self,
                 head=None,
                 tail=None):

        self.head = head
        self.tail = tail
        self._length = 0

    def __str__(self):
        return f"Sll with length {self.length}"

    # Length instance attribute
    # "read-only" length property
    @property
    def length(self):
        return self._length

    '''
    Traverse: This method traverses the linked list to find
    the node at a given index.
    If the index is negative, it is interpreted as counting from
    the end of the list.
    If the index is out of bounds, an IndexError is raised.
    If the index is not an integer, a TypeError is raised.
    This method returns the node at the given index.
    '''

    # Traverse
    def traverse(self, idx):

        # handle negative idx
        if idx < 0:
            idx == self._length + idx

        # If list empty
        # If idx out of bounds
        if (
            self._length == 0 or
            idx > (self._length - 1) or
            idx < 0
        ):
            raise IndexError("idx out of range")

        # If idx not an integer
        if not isinstance(idx, int):
            raise TypeError("idx must be an integer")

        i = 0
        _current_node = self.head

        while i < idx:
            _current_node = _current_node.link
            i += 1

        return _current_node

    '''
    Insert: This method inserts a new node with a given value at a given
    index in the linked list. If the index is not specified, the new node
    is inserted at the end of the list. If the index is negative, it is
    interpreted as counting from the end of the list.
    If the index is out of bounds, an IndexError is raised.
    This method returns None.
    '''

    def insert(self, val, idx=None):

        _new_node = MyNode(val, link=None)

        # idx None (default)
        if idx is None:
            idx = self._length

        # handle negative indices
        if (idx < 0):
            idx = self._length + idx

        # list empty, inserting first node
        if self.head is None:
            self.head = _new_node
            self.tail = _new_node
            self._length += 1
            return None

        # insert at tail (default)
        elif (idx == self._length):
            self.tail.link = _new_node
            self.tail = _new_node
            self._length += 1
            return None

        # insert before head
        elif idx == 0:
            _new_node.link = self.head
            self.head = _new_node
            self._length += 1
            return None

        # insert between two nodes
        else:

            this_idx_node = self.traverse(idx - 1)
            next_idx_node = this_idx_node.link

            _new_node.link = next_idx_node
            this_idx_node.link = _new_node

            self._length += 1
            return None

    '''
    Remove: This method removes the node at a given index from the
    linked list and returns its value. If the index is negative,
    it is interpreted as counting from the end of the list.
    If the index is out of bounds, an IndexError is raised.
    If the index is not an integer, a TypeError is raised.
    If the list is empty, an IndexError is raised.
    '''

    def remove(self, idx=0):

        # handle negative indicies
        if (idx < 0):
            idx = self._length + idx

        # If list empty
        # If idx out of bounds
        if (
            self._length == 0 or
            idx > (self._length - 1) or
            idx < 0
        ):
            raise IndexError("idx out of range")

        # If idx not an integer
        if not isinstance(idx, int):
            raise TypeError("idx must be an integer")

        # If idx 0
        if idx == 0:
            _val = self.head.value
            self.head = self.head.link
            self._length -= 1

            # If no other members
            if self._length == 0:
                self.tail = None

            return _val

        # other indices
        previous_idx_node = self.traverse(idx - 1)
        this_idx_node = previous_idx_node.link

        _val = this_idx_node.value

        next_idx_node = this_idx_node.link

        # connect previous to next
        previous_idx_node.link = next_idx_node

        if next_idx_node is None:
            self.tail = previous_idx_node

        self._length -= 1
        return _val

    def search(self, given_val):
        """
        search
        if given_val is in the sll, return the idx
        of the first occurrence
        else return -1

        The search method searches for the first occurance of a
        given value in the list and returns its index. If the value
        is not found, it returns -1.
        """

        current_node = self.head
        i = 0

        while current_node is not None:
            if current_node.value == given_val:
                return i
            current_node = current_node.link
            i += 1

        return -1
