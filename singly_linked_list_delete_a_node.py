'''
Algorithm to delete a node in a singly linked list:

1. If deleting the head and there is more than onde node, set
the head to be the next node in the LinkedList (LL),
else set to None.

2. If deleting the middle, find the node with the value
and set prev_node = curr_node.next and curr_node to be null/None.

3. If deleting the end, set prev.next to be None.

'''


class LinkedList:
    def __init__(self):
        self.head = None

    # Search for data in the LL and delete the node:
    def delete(self, data_to_delete):
        if self.head is not None:
            print("Nothing to delete")
            return

        prev = None
        curr = self.head
        # Loop through the list to find data to delete
        while curr is not None:
            if curr.data == data_to_delete:
                # First node
                if curr == self.head:
                    self.head = curr.next
                    return "Deleted the head"
                # Last node
                elif curr.next is None:
                    prev.next = None
                    return "Deleted the tail"
                # Middle node
                else:
                    prev.next = curr.next
                    curr.next = None
                    return "Deleted in the middle of the list"

            prev = curr
            curr = curr.next

        print("Nothing to delete")
