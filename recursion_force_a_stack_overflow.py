def my_factorial(n, a=1):
    # base case
    if n in [0, 1]:
        return 1 * a
    else:
        # recursive case
        return my_factorial(n - 1, a * n)


# increase the value of num
# until you get an error
num = 5

while True:
    try:
        print(my_factorial(num))
        num += 1
    except RecursionError:
        print(f"Maximum recursion depth exceeded at num = {num}")
        break
