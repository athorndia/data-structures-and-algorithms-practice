# Node implementation for a singly-linked list
class Node:

    def __init__(self, data, next=None):
        self.data = data
        self.next = next

# Singly Linked List implementation


class LinkedList:
    def __init__(self, head=None, tail=None):
        self.head = head
        self.tail = tail

    def printList(self):
        pointer = self.head

        while (pointer is not None):
            print(pointer.data, end="-->")
            pointer = pointer.next

    def add(self, data):
        if self.head is None:
            self.head = Node(data)
            self.tail = self.head
        else:
            self.tail.next = Node(data)
            self.tail = self.tail.next

    def size(self):
        count = 0
        pointer = self.head

        while (pointer is not None):
            count = count + 1
            pointer = pointer.next

        print("The size of the Linked List is:",  count)

    def delete_second_to_last(self):
        print("TODO: Implement the code to delete the second to last node")


if __name__ == '__main__':
    list = LinkedList()
    list.add(1)
    list.add(2)
    list.add(3)
    list.add(4)

    list.size()

    # When called for this list, the output should be 1->2->4->
    list.delete_second_to_last()

    list.printList()
