'''
Question stem:
A triangular number counts objects arranged in an equilateral triangle.

The function below returns the nth triangular number
utilizing an iterative approach.

Refactor the code so that it uses a recursion to achieve the same results.

The function will be tested with positive integers including 0.
'''


# OLD
# def triangular(n):
#     acc = 0
#     for i in range(n + 1):
#         acc += i
#     return acc

# NEW
def triangular(n):
    if n == 0:
        return 0
    else:
        return n + triangular(n - 1)


'''
The triangular number of 'n' is the sum of all positive integers up
to and including 'n'.

    If n == 0, return 0

Else, use recursion to call itself with 'n-1' as the input
AND adds 'n' to the result.

    return triangular(n - 1) + n

This process continues until the base case of 'n=0' is reached.
'''
