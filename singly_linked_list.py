# Node implementation for a singly-linked list


class Node:
    '''
    Attributes data and next represent fundamental building blocks
    of a linked list. In a singly-linked list, each node contains two
    pieces of info:
        - the data that the node is storing (i.e., int, str, obj, etc.)
        - and a reference to the next node in the list.
    '''
    def __init__(self, data, next=None):
        self.data = data
        self.next = next


'''
Using the Node class with those two attributes we can create a
chain of nodes that are linked together, with each node storing
a piece of data and a reference to the next node in the list.

This allows us to traverse the list by following the next
reference from one node to the next.
'''

# Singly Linked List implementation


class LinkedList:
    def __init__(self, head=None, tail=None):
        '''
        The head attribute represents the first node in the list,
        while the tail attribute represents the last node in the list.

        By keeping track of these two attributes, we can efficiently
        add new nodes to the end of the list (by updating the next reference
        to the current tail node), and traverse the list from beginning to end
        (by following the next references from one node to the next,
        starting at the head)
        '''
        self.head = head
        self.tail = tail

    # printList method:
    def printList(self):
        # we initalize a pointer variable with the head of the linked list
        pointer = self.head

        # the while loop iterates as long as the pointer is not None
        while pointer is not None:
            # if true:
            # we print the data of the current node pointed by the pointer,
            # followed by an arrow (-->) to indicate the connection
            # to the next node.
            print(pointer.data, end="-->")
            # we update the pointer to point to the next node in the
            # list (pointer.next)
            pointer = pointer.next

    # add method:
    def add(self, data):
        print("TODO: Implement add with the tail")
        # we create a new node object (new_node) with the provided data.
        new_node = Node(data)
        # if the linked list is empty (i.e., self.head is None)
        if self.head is None:
            # we set both self.head and self.tail to the new node
            self.head = new_node
            self.tail = self.head
        else:
            self.tail.next = new_node
            self.tail = self.tail.next

    # size method:

    def size(self):
        # we initialize a count variable to keep track of the number of
        # nodes in the linked list.
        count = 0
        # we initalize a pointer variable with the head of the linked list.
        pointer = self.head
        print("TODO: Implement size function that prints size of Linked List")
        # we enter a while loop that iterates as long as the
        # pointer is not None
        while pointer is not None:
            # inside the loop, we increment the count by 1 and
            count += 1
            # update the pointer to the next node in the list (pointer.next)
            pointer = pointer.next
        # After exiting the loop we print the value of count,
        # which represents the size of the linked list
        print("Size of LinkedList:", count)

    # Assumes LL has at least 2 nodes!

    def delete_second_to_last(self):
        print("TODO: Implement the code to delete the second to last node")
        if self.head is None or self.head.next is None:
            # Check if the linked list has 0 or 1 nodes.
            # If it does, we can't delete the 2nd to last node, we print
            # an error message and return
            print(
                "Linked list has 0 or 1 nodes. Cannot delete 2nd to last node."
            )
            return

        # Next, initialize a pointer variable to the head of the linked list,
        pointer = self.head
        # use while loop to iterate through the list until we
        # find the second to last node
        # (i.e., the node whose next.next attribute is None)
        while pointer.next.next is not None:
            # once found the 2nd to last node, we simply set
            # its next attribute to None,
            # effectively deleting the last node in the list.
            pointer = pointer.next

        # Delete the second to last node
        pointer.next = None


'''
The (if __name__ == '__main__':) is a conditional statement that checks
whether the script is being run as the main program or if it's being
imported as a module into another program.

If the script is being run as the main program '__name__' takes on the value of
'__main__'.

If the script is being imported as a module into another program,
then '__name__' is set to the name of the module.
'''

# LinkedList implmentation:
if __name__ == '__main__':
    # create a new LinkedList object called list
    list = LinkedList()
    # add nodes to the list using the add method
    list.add(1)
    list.add(2)
    list.add(3)

    # print the size of tthe list using the size method:
    # If size is implemented correctly,
    # you should see an output of 3
    list.size()

    # print the contents of the list using the printList method:
    # If add is implemented correctly,
    # you should see an output of 1->2->3
    list.printList()
